# Helm Template Language

[![GoDoc](http://godoc.org/gitlab.com/CRThaze/helm-tmpl-lang?status.svg)](http://godoc.org/gitlab.com/CRThaze/helm-tmpl-lang)

A convenience wrapper for using Helm's templating engine as a generic template
dialect, outside of a Helm Chart.

## Usage

```go
import (
	htl "gitlab.com/CRThaze/helm-tmpl-lang"
)
```
