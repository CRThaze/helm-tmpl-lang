package helm_tmpl_lang

import (
	"os"
	"path/filepath"

	"helm.sh/helm/v3/pkg/chart"
	"helm.sh/helm/v3/pkg/chartutil"
	"helm.sh/helm/v3/pkg/engine"
)

// LangOpts are the settings for how the Helm Templating Engine works.
type LangOpts struct {
	Strict    bool
	LintMode  bool
	EnableDNS bool
}

var defaultLangOpts = LangOpts{
	Strict:    true,
	LintMode:  false,
	EnableDNS: true,
}

// Templater renders templates using the Helm Templating Engine.
type Templater struct {
	engine engine.Engine
	chart  chart.Chart
}

// Render takes values and returns a map of rendered templates.
func (t *Templater) Render(values map[string]interface{}) (map[string]string, error) {
	t.chart.Values = values
	return t.engine.Render(&t.chart, chartutil.Values(values))
}

// NewTemplaterWithLangOpts creates a new Templater with optional custom
// Helm Templating Language options.
func NewTemplaterWithLangOpts(
	templates map[string][]byte,
	langOpts *LangOpts,
) *Templater {
	if langOpts == nil {
		langOpts = &defaultLangOpts
	}

	if templates == nil {
		templates = map[string][]byte{
			"": []byte{},
		}
	}

	ht := &Templater{
		engine: engine.Engine{
			Strict:    langOpts.Strict,
			LintMode:  langOpts.LintMode,
			EnableDNS: langOpts.EnableDNS,
		},
		chart: chart.Chart{
			Raw:       make([]*chart.File, 0, 0),
			Metadata:  &chart.Metadata{},
			Lock:      &chart.Lock{},
			Templates: make([]*chart.File, len(templates), len(templates)),
			Values:    nil,
			Schema:    make([]byte, 0, 0),
			Files:     make([]*chart.File, 0, 0),
		},
	}

	var i int
	for name, data := range templates {
		ht.chart.Templates[i] = &chart.File{
			Name: name,
			Data: data,
		}
		i++
	}
	return ht
}

// NewTemplater creates a Templater instance with default Helm Templating Language options.
func NewTemplater(templates map[string][]byte) *Templater {
	return NewTemplaterWithLangOpts(templates, nil)
}

// NewTemplaterWithFiles creates a Templater instance with with a slice of file paths
// containing templates to render, along with optional Helm Templating Language options.
func NewTemplaterWithFiles(
	tplFiles []string,
	langOpts *LangOpts,
) (*Templater, error) {
	tpls := map[string][]byte{}
	var err error
	for _, tf := range tplFiles {
		tpls[filepath.Base(tf)], err = os.ReadFile(tf)
		if err != nil {
			return nil, err
		}
	}
	return NewTemplaterWithLangOpts(tpls, langOpts), nil
}

// NewTemplaterWithStr creates a Templater instance with a single template as a string,
// along wit optional Helm Templating Language options.
func NewTemplaterWithStr(
	tplStr string,
	langOpts *LangOpts,
) *Templater {
	return NewTemplaterWithLangOpts(
		map[string][]byte{
			"": []byte(tplStr),
		},
		langOpts,
	)
}

// NewTemplaterWithBytes creates a Templater instance with a single template as a slice of bytes,
// along wit optional Helm Templating Language options.
func NewTemplaterWithBytes(
	tplBytes []byte,
	langOpts *LangOpts,
) *Templater {
	return NewTemplaterWithLangOpts(
		map[string][]byte{
			"": tplBytes,
		},
		langOpts,
	)
}
